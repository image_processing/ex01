import numpy as np
import matplotlib.pyplot as plt
from scipy import misc
from skimage import color
from sklearn.cluster import KMeans  # Using this import only in the bonus question

RGB_CHANNELS = 3
GRAYS_SCALE = 1
COLOR = 2
TRANS_MAT = yiq_from_rgb = np.array([[0.299, 0.587, 0.114],
                                     [0.59590059, -0.27455667, -0.32134392],
                                     [0.21153661, -0.52273617, 0.31119955]])


def read_image(filename, representation):
    """
    :param filename - string containing the image filename to read.
    :param representation - representation code, either 1 or 2 defining whether the output should be a grayscale 
    :return:  an image, make sure the output image is represented by a matrix of type
    np.float64 with intensities (either grayscale or RGB channel intensities) normalized to the range [0, 1].
    """

    pic = np.float64(misc.imread(filename, mode='RGB') / 255)
    if representation == GRAYS_SCALE:
        return color.rgb2gray(pic).astype(np.float64)
    else:
        return pic


def imdisplay(filename, representation):
    """
    :param filename - string containing the image filename to read.
    :param representation - representation code, either 1 or 2 defining whether the output should be a grayscale 
    :return: 
    """
    plt.figure()
    img = read_image(filename, representation)
    if representation == GRAYS_SCALE:
        plt.imshow(img, cmap="gray")
    else:
        plt.imshow(img)
    plt.show()


def rgb2yiq(imRGB):
    """
    transform rgb image to yiq image
    :param imRGB: 
    :return: 
    """
    return np.dot(imRGB, TRANS_MAT.transpose())


def yiq2rgb(imYIQ):
    """
    transform yiq image to rgb image.
    :param imYIQ: 
    :return: 
    """

    return np.dot(imYIQ, np.linalg.inv(TRANS_MAT).transpose())


def histogram_equalize(im_orig):
    """
    :param im_orig - is the input grayscale or RGB float64 image with values in [0, 1].
    :return:  The function returns a list [im_eq, hist_orig, hist_eq] where
    im_eq - is the equalized image. grayscale or RGB float64 image with values in [0, 1].
    hist_orig - is a 256 bin histogram of the original image (array with shape (256,) ).
    hist_eq - is a 256 bin histogram of the equalized image (array with shape (256,) ).
    """

    is_rgb = im_orig.ndim == 3
    if is_rgb:  # if is RGB
        yiq_img = rgb2yiq(im_orig)
        im2eq = yiq_img[:, :, 0].copy()
    else:
        im2eq = im_orig.copy()

    im2eq = np.multiply(im2eq, 255).round().astype(np.uint8)

    # create the cumulative histogram and normalize it
    hist_orig, _ = np.histogram(im2eq, bins=256)
    cumhist = np.cumsum(hist_orig)
    cumhist = np.multiply(cumhist, (255 / cumhist[-1]))

    # create lookup table and use on image to get the equalized image
    lookup_tbl = get_normelized_lookup_tbl(cumhist)
    im_eq = lookup_tbl[im2eq]
    hist_eq, _ = np.histogram(im_eq, bins=256)
    im_eq = (im_eq.astype(np.float64)) / 255.0

    if is_rgb:
        yiq_img[:, :, 0] = im_eq
        im_eq = np.clip(yiq2rgb(yiq_img), 0, 1)

    return [im_eq, hist_orig, hist_eq]


def get_normelized_lookup_tbl(cumhist):
    sm = np.min(cumhist[np.nonzero(cumhist)])
    s_max = np.max(cumhist)
    lookup_tbl = np.round(255 * (cumhist - sm) / (s_max - sm))
    return lookup_tbl


def quantize_gray(gray_orig, n_quant, n_iter):
    """
    Quantize gray image (2 dim array)
    :param gray_orig: 
    :param n_quant: 
    :param n_iter: 
    :return: a list [im_quant, error] where
    im_quant - is the quantized output image.
    error - is an array with shape (n_iter,) (or less) of the total intensities error for each iteration of the
    quantization procedure.
    """
    segments = np.empty(n_quant + 1, dtype=np.uint)
    segments[0], segments[-1] = 0, 255

    im2quant = np.multiply(gray_orig, 255).round().astype(np.uint8)
    hist_orig, _ = np.histogram(im2quant, bins=range(257))

    #  use cum histogram to get the segments to divide the pic properly
    cumsum_orig = np.cumsum(hist_orig)
    avg_pxl_num = cumsum_orig[-1] / n_quant
    for i in range(1, n_quant):
        segments[i] = np.where(cumsum_orig >= i * avg_pxl_num)[0][0]

    error, qs = quant_algo_run(cumsum_orig, hist_orig, n_iter, n_quant, segments)

    lookup_tbl = np.arange(256, dtype=np.uint8)
    for i in range(n_quant):
        start, end = segments[i], segments[i + 1] if  segments[i + 1] < 255 else 256
        lookup_tbl[start:end] = qs[i]

    im_quant = np.divide(lookup_tbl[im2quant].astype(np.float64), 255)
    return [im_quant, error]


def quant_algo_run(cumsum_orig, hist_orig, n_iter, n_quant, segments):
    """
    Performs the algorithm for quantize image
    :param cumsum_orig: cumsum of orig image
    :param hist_orig: histogram of orig image
    :param n_iter:  max number of iteration to perform
    :param n_quant: numbr of quant wanted 
    :param segments: segments in img
    :return: error, qs where
    error: An array of size k which is the actuall number of iterations the algorithm performed before convergance. each
    cell holding the error for the corresponding iteration
    qs : the values for the colors the algorithm found
    """
    qs = np.empty(n_quant)
    # use this as the Sum(
    z_hist = np.arange(256) * hist_orig
    z_sum = np.cumsum(z_hist)
    error = np.array([], dtype=np.float64)
    old_segments = np.empty_like(segments)
    for j in range(n_iter):

        # update q
        for i in range(n_quant):
            start = segments[i]
            end = segments[i + 1]
            weighted_pxls = z_sum[end] - z_sum[start]
            pixles_in_range = cumsum_orig[end] - cumsum_orig[start]
            if pixles_in_range == 0:
                continue
            qs[i] = (weighted_pxls / pixles_in_range)

        # update segments (s)
        for i in range(1, n_quant):
            segments[i] = (np.divide((qs[i - 1] + qs[i]), 2)).astype(np.uint)

        # calc error
        error = np.append(error, calc_quant_err(hist_orig, n_quant, qs, segments))

        # break loop if no change has been made to the segments
        if np.array_equal(segments, old_segments):
            break
        else:
            old_segments = np.copy(segments)

    return error, qs


def calc_quant_err(hist_orig, n_quant, qs, segments):
    """
    get squared error for a givent image histogram, quants, q, segments
    :param hist_orig: original histogram of image.
    :param n_quant: number of quants in image
    :param qs: The q (values)
    :param segments: The segments for the image
    :return: 
    """

    err = 0
    # !! there is a problem in lab linux when doing segments[i]+1, sometimes it makes it a float instead of int. force type
    for i in range(n_quant):
        start = int(segments[i] + 1 if segments[i] > 0 else 0)
        end = int(segments[i + 1] if segments[i+1] < 255 else 256)
        diff_vals = qs[i] - np.arange(start, end)
        sqr_diff = np.square(diff_vals) * hist_orig[start:end]
        err += sqr_diff.sum()
    return err


def quantize_gray_from_rgb(im_orig, n_quant, n_iter):
    """
    Quantize rgb image using transfaring to YIQ and performing quantize on the Y and back to rgb
    :param im_orig: rgb image
    :param n_quant: number of quants for the output image
    :param n_iter: max number of iteration to perform
    :return: a list [im_quant, error] where
    im_quant - is the quantized output image.
    error - is an array with shape (n_iter,) (or less) of the total intensities error for each iteration of the
    quantization procedure.
    """
    im_quant = im_orig.copy()
    yiq_img = rgb2yiq(im_quant)
    im2qnt = yiq_img[:, :, 0].copy()
    gray_im_quant, error = quantize_gray(im2qnt, n_quant, n_iter)
    yiq_img[:, :, 0] = gray_im_quant
    im_quant = yiq2rgb(yiq_img)
    return [im_quant, error]


def quantize(im_orig, n_quant, n_iter):
    """
    :param  im_orig - is the input grayscale or RGB image to be quantized (float64 image with values in [0, 1]).
    :param  n_quant - is the number of intensities your output im_quant image should have.
    :param  n_iter - is the maximum number of iterations of the optimization procedure (may converge earlier.) 
    :return: a list [im_quant, error] where
    im_quant - is the quantized output image.
    error - is an array with shape (n_iter,) (or less) of the total intensities error for each iteration of the
    quantization procedure.
    """
    if im_orig.ndim == RGB_CHANNELS:
        return quantize_gray_from_rgb(im_orig, n_quant, n_iter)
    else:
        return quantize_gray(im_orig, n_quant, n_iter)


def quantize_rgb(im_orig, n_quant, n_iter):
    """
    Bonus part
    This function runs the quantization algorithm on the given RGB image.
    :param im_orig: is the input RGB image to be quantized (float64 image with values in [0, 1]).
    :param n_quant:  is the number of intensities your output im_quant image should have.
    :param n_iter: is the maximum number of iterations of the optimization procedure (may converge earlier.) 
    :return: a list [im_quant, error] where
    im_quant - is the quantized output image.
    error - is an array with shape (n_iter,) (or less) of the total intensities error for each iteration of the
    quantization procedure.
    """
    im2quant = im_orig.copy()
    im2quant = im2quant.reshape(-1, 3)
    error = np.array([], dtype=np.float64)
    kmeans = KMeans(n_clusters=n_quant, max_iter=1).fit(im2quant)
    old_clusters = kmeans.cluster_centers_
    error = np.append(error, rgb_quant_err(old_clusters, kmeans.labels_, im_orig))

    # iterate, each time passing the last clusters for the init and calc the error.
    for i in range(n_iter - 1):
        kmeans = KMeans(n_clusters=n_quant, n_init=1, max_iter=1, init=kmeans.cluster_centers_).fit(im2quant)
        clusters = kmeans.cluster_centers_
        if np.array_equal(clusters, old_clusters):
            break
        else:
            old_clusters = clusters.copy()
        error = np.append(error, rgb_quant_err(kmeans.cluster_centers_, kmeans.labels_, im_orig))

    im2quant = kmeans.cluster_centers_[kmeans.labels_]
    im2quant = im2quant.reshape(im_orig.shape)

    return im2quant, error


def rgb_quant_err(cluster_centers, labels, im_orig):
    im_quant = cluster_centers[labels]
    im_quant = im_quant.reshape(im_orig.shape)
    diff = np.sqrt(np.sum(np.square(im_quant - im_orig), axis=2))
    return np.sum(np.sum(diff, axis=0))
