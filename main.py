from sol1 import *


def test_histEQ():
    pic_path = r"C:\Users\fgdvi\Google Drive\University\2017 - 2018\Image_Processing\ex01\jerusalem.jpg"
    # pic_path = r"C:\Users\fgdvi\Google Drive\University\2017 - 2018\Image_Processing\ex01\girl.png"
    # pic_path = r"C:\Users\fgdvi\Google Drive\University\2017 - 2018\Image_Processing\ex01\rgb_orig.png"
    # pic_path = r"C:\Users\fgdvi\Google Drive\University\2017 - 2018\Image_Processing\ex01\test.jpg"
    # pic_path = r"C:\Users\fgdvi\Google Drive\University\2017 - 2018\Image_Processing\ex01\Low Contrast.jpg"
    img = read_image(pic_path, 2)
    # img = misc.imread(pic_path)
    # im_eq, hist_orig, hist_eq = histogram_equalize(img)
    iters = 30000
    im_quant, err = quantize(img, 6, iters)
    im_quant2, err2 = quantize(img, 6, iters)
    im_quant3, err3 = quantize(img, 10, iters)
    # plt.hist(img.ravel(),256,range=[0,256])
    plt.figure()
    # plt.hist(im_eq.ravel(),256,range=[0,256])
    # plt.imshow(im_eq,cmap='gray')
    plt.imshow(np.clip(im_quant, 0, 1), cmap='gray')
    plt.show()
    plt.figure()
    # plt.imshow(np.clip(im_quant2, 0, 1), cmap='gray')
    # plt.figure()
    # plt.imshow(np.clip(im_quant3, 0, 1), cmap='gray')
    # plt.figure()
    # y = np.arange(len(err3))
    # plt.plot(y, err3, )
    #
    #
    # img_adapteq = exposure.equalize_adapthist(img)
    # plt.imshow(img_adapteq)


def quant():
    # pic_path = r"C:\Users\fgdvi\Google Drive\University\2017 - 2018\Image_Processing\ex01\Low Contrast.jpg"
    pic_path = r"C:\Users\fgdvi\Google Drive\University\2017 - 2018\Image_Processing\ex01\files\jerusalem.jpg"
    # pic_path = r"C:\Users\fgdvi\Google Drive\University\2017 - 2018\Image_Processing\ex01\monkey.jpg"
    # pic_path = r"C:\Users\fgdvi\Google Drive\University\2017 - 2018\Image_Processing\ex01\DSCF6721.jpg"
    img = read_image(pic_path, 2)
    # quant_im, err = quantize_rgb(img, 16, 1000)
    quant_im, err = quantize(img, 100, 100)
    plt.imshow(np.clip(quant_im, 0, 1),cmap='gray')
    plt.figure()
    y = np.arange(len(err))
    plt.title("Dvir")
    plt.plot(y, err)
    plt.show()


def check_hist():
    # pic_path = r"C:\Users\fgdvi\Google Drive\University\2017 - 2018\Image_Processing\ex01\Low Contrast.jpg"
    # pic_path = r"C:\Users\fgdvi\Google Drive\University\2017 - 2018\Image_Processing\ex01\DSCF6748.jpg"
    # pic_path = r"C:\Users\fgdvi\Google Drive\University\2017 - 2018\Image_Processing\ex01\original.png"
    pic_path = r"C:\Users\fgdvi\Google Drive\University\2017 - 2018\Image_Processing\ex01\girl.png"
    img = read_image(pic_path, 2)
    im_eq, hist_orig, hist_eq = histogram_equalize(img)
    plt.imshow(im_eq, cmap='gray')
    plt.figure()
    plt.hist(hist_orig, 256)
    plt.title("hist_orig")
    plt.figure()
    plt.hist(hist_eq, 256)
    plt.title("hist_eq")
    plt.show()
    pass

def display_img():
    pic_path = r"C:\Users\fgdvi\Google Drive\University\2017 - 2018\Image_Processing\ex01\Low Contrast.jpg"
    imdisplay(pic_path,2)
    imdisplay(pic_path,1)



if __name__ == '__main__':
    quant()
    # check_hist()
    # display_img()
    # pres.presubmit()
